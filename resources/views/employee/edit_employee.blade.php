@extends('layout.main')
@section('content')
    
<div class="container-fluid">
                        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                    <a href="{{route('employee.table')}}" class="btn btn-danger"><i class="fas fa-chevron-left"></i> Go Back</a>
                    </div>
                    <h4 class="page-title">Edit Employee</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                      
                        <div class="row">
                            <div class="col-lg-12">
                            <form method="POST" action="{{route('update.employee')}}" >
                                    {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$employee->employee_id}}">
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">First Name</label>
                                        <input type="text" id="simpleinput" class="form-control" name="first_name" value="{{$employee->first_name}}">
                                        </div>
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">Last Name</label>
                                        <input type="text" id="simpleinput" class="form-control" name="last_name" value="{{$employee->last_name}}">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="example-select">Company</label>
                                            <select class="form-control" id="example-select" name="company">
                                                @foreach($company as $companies)
                                                <option value="{{ $companies->company_id }}" @if($companies->company_id == $employee->company_id) selected @endif>{{ $companies->company_name}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">Email</label>
                                        <input type="text" id="simpleinput" class="form-control" name="email" value="{{$employee->email}}">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="example-number">Phone Number</label>
                                        <input class="form-control" type="text" name="phone" maxlenght="12" value="{{$employee->phone}}">
                                        </div>
                                        <button class="btn btn-outline-success waves-effect waves-light">Update Employee Details</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection