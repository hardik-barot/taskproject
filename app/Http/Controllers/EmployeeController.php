<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeModel;
use App\CompanyModel;
use Validator;

class EmployeeController extends Controller
{
        public function add_employee()
        {
            $companies=CompanyModel::all();

            return view('employee.add')->with('companies',$companies);
        }
        public function insert(Request $request)
        {
           
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name'=>'required',
                'email'=>'required',
                'phone'=>'required'
                
            ]);
    
            if ($validator->fails()) {
                return redirect()->route('employee.add')->withErrors($validator);
            }

            $company= new EmployeeModel;
            $company->first_name=$request->first_name;
            $company->last_name=$request->last_name;
            $company->company_id=$request->company;
            $company->email=$request->email;
            $company->phone=$request->phone;
            $company->save();

            return redirect()->route('employee.table')->with('success','Employee Added Successfully');
        }
        public function table()
        {
            return view('employee.table');
        }
        public function employeeDatatable(Request $request)
        {
            $employees = EmployeeModel::query();
            $employees->with('company');
            $recordsTotal = $employees->count();
    
                    $searchParam = $request->search['value'] ?? "";
                    $employees->when($searchParam, function($query) use($searchParam){
                    return $query->where('first_name','like','%'.$searchParam.'%');
                    });
                    
                    
                    $recordsFiltered = $employees->get()->count();
                    $employees->offset($request->start)->limit($request->length)->orderBy('employee_id','DESC');
    
                    $data = [
                        'draw' => $request->draw,
                        'recordsTotal' => $recordsTotal,
                        'recordsFiltered' => $recordsFiltered,
                        'data' => $employees->get()
                    ];
        return $data;
    
    
        }
        public function edit_employee($id)
        {
            $company=CompanyModel::all();
            $employee=EmployeeModel::with('company')->find($id);

            $data['company']=$company;
            $data['employee']=$employee;

            return view('employee.edit_employee')->with($data);

        }
        public function update_employee(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name'=>'required',
                'company'=>'required',
                'email'=>'required',
                'phone'=>'required'
            ]);
    
            if ($validator->fails()) {
                return redirect()->route('edit.employee',['id'=>$request->id])->withErrors($validator);
            }

            $employee= EmployeeModel::find($request->id);
            $employee->first_name=$request->first_name;
            $employee->last_name=$request->last_name;
            $employee->company_id=$request->company;
            $employee->email=$request->email;
            $employee->phone=$request->phone;
            $employee->save();

            return redirect()->route('employee.table')->with('success','Employee Details Has Been Updated Successfully.');
        }
        public function delete_employee($id)
        {
            EmployeeModel::find($id)->delete();
            return redirect()->route('employee.table')->with('success','Employee Deleted Successfully');
        }
}
