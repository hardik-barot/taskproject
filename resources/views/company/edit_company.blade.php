@extends('layout.main')
@section('content')
    
<div class="container-fluid">
                        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                    <a href="{{route('company.table')}}" class="btn btn-danger"><i class="fas fa-chevron-left"></i> Go Back</a>
                    </div>
                    <h4 class="page-title">Edit Company</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                      
                        <div class="row">
                            <div class="col-lg-12">
                            <form method="POST" action="{{route('update.company')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$company->company_id}}">
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">Company Name</label>
                                        <input type="text" id="simpleinput" class="form-control" name="company_name" value="{{$company->company_name}}" >
                                        </div>
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">Email</label>
                                                <input type="text" id="simpleinput" class="form-control" name="email" value="{{$company->email}}" >
                                        </div>
                                        <div class="form-group mb-3">
                                                <label for="example-fileinput">Logo</label>
                                                <input type="file" id="example-fileinput" class="form-control-file" name="logo" value="{{$company->logo}}" >
                                        
                                        </div>
                                        <div class="form-group mb-3">
                                                <label for="simpleinput">Company URL</label>
                                                <input type="text" id="simpleinput" class="form-control" name="website_url" value="{{$company->website_url}}" >
                                        </div>
                                        <button class="btn btn-outline-success waves-effect waves-light">Update</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection