<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from coderthemes.com/ubold/layouts/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Dec 2019 13:15:35 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Task</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('/images/favicon.ico')}}">

        <!-- Plugins css -->
        <link href="{{asset('/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{asset('/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <ul class="list-unstyled topnav-menu float-right mb-0">


                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="{{asset('/images/admin1.jpg')}}" alt="user-image" class="rounded-circle">
                            <span class="pro-user-name ml-1">
                                Admin <i class="mdi mdi-chevron-down"></i> 
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>

                            <!-- item-->
                    

                            <!-- item-->
                        <a href="{{route('logout')}}" onclick="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="fe-log-out"></i>
                                <span>Logout</span>
                            </a>

                        </div>
                    </li>

                    {{-- <li class="dropdown notification-list">
                        <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                            <i class="fe-settings noti-icon"></i>
                        </a>
                    </li> --}}


                </ul>

                <!-- LOGO -->
                <div class="logo-box">
                    <a href="index.html" class="logo text-center">
                        <span class="logo-lg">
                            <img src="{{asset('/images/logo-dark.png')}}" alt="" height="18">
                            <!-- <span class="logo-lg-text-light">UBold</span> -->
                        </span>
                        <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">U</span> -->
                            <img src="{{asset('/images/admin1.jpg')}}" alt="" height="24">
                        </span>
                    </a>
                </div>

                <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                    <li>
                        <button class="button-menu-mobile waves-effect waves-light">
                            <i class="fe-menu"></i>
                        </button>
                    </li>
        
                  

                </ul>
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">Navigation</li>

                            {{-- <li>
                                <a href="javascript: void(0);">
                                    <i class="fe-airplay"></i>
                                   
                                    <span> Dashboards </span>
                                </a>
                                
                            </li> --}}

                            <li>
                            <a href="{{route('company.table')}}">
                                    <i class="fe-briefcase"></i>
                                    <span> Company </span>
                                    
                                </a>
                               
                            </li>
                            <li>
                                <a href="{{route('employee.table')}}">
                                    <i class="fas fa-smile"></i>
                                        <span> Employee </span>
                                        
                                    </a>
                                   
                                </li>

                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                        @include('alert.alert')
                        @yield('content')
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <!-- Footer Start -->
                {{-- <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                
                            </div>
                           
                        </div>
                    </div>
                </footer> --}}
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        {{-- <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="dripicons-cross noti-icon"></i>
                </a>
                <h5 class="m-0 text-white">Settings</h5>
            </div>
            <div class="slimscroll-menu">
                <!-- User box -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
                    </div>
            
                    <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
                    <p class="text-muted mb-0"><small>Admin Head</small></p>
                </div>

                <!-- Settings -->
                <hr class="mt-0" />
                <h5 class="pl-3">Basic Settings</h5>
                <hr class="mb-0" />

                <div class="p-3">
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox1" type="checkbox" checked>
                        <label for="Rcheckbox1">
                            Notifications
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox2" type="checkbox" checked>
                        <label for="Rcheckbox2">
                            API Access
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox3" type="checkbox">
                        <label for="Rcheckbox3">
                            Auto Updates
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox4" type="checkbox" checked>
                        <label for="Rcheckbox4">
                            Online Status
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-0">
                        <input id="Rcheckbox5" type="checkbox" checked>
                        <label for="Rcheckbox5">
                            Auto Payout
                        </label>
                    </div>
                </div>

                <!-- Timeline -->
                <hr class="mt-0" />
                <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
                <hr class="mb-0" />
                <div class="p-3">
                    <div class="inbox-widget">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-2.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Tomaslau</a></p>
                            <p class="inbox-item-text">I've finished it! See you so...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Stillnotdavid</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Kurafire</a></p>
                            <p class="inbox-item-text">Nice to meet you</p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-5.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Shahedk</a></p>
                            <p class="inbox-item-text">Hey! there I'm available...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-6.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Adhamdannaway</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                    </div> <!-- end inbox-widget -->
                </div> <!-- end .p-3-->

            </div> <!-- end slimscroll-menu-->
        </div> --}}
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('/js/vendor.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{asset('/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('/libs/datatables/dataTables.select.min.js')}}"></script>
        <script src="{{asset('/js/pages/datatables.init.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('/libs/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{asset('/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('/libs/flot-charts/jquery.flot.js')}}"></script>
        <script src="{{asset('/libs/flot-charts/jquery.flot.time.js')}}"></script>
        <script src="{{asset('/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('/libs/flot-charts/jquery.flot.selection.js')}}"></script>
        <script src="{{asset('/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{asset('/js/pages/dashboard-1.init.js')}}"></script>

        <!-- App js-->
        <script src="{{asset('/js/app.min.js')}}"></script>
        @yield('scripts')
    </body>

<!-- Mirrored from coderthemes.com/ubold/layouts/dark/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Dec 2019 13:17:53 GMT -->
</html>