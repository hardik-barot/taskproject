@extends('layout.main')
@section('content')
<div class="container-fluid">
                        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                    <a href="{{route('employee.add')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Add Employee</a>
                    </div>
                    <h4 class="page-title">List Of Employees</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title"></h4>
                       

                        <table id="datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody></tbody>

                            </table>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">

    $(document).ready(function(){
            // get_songs();



                    let datatable = $('#datatable').DataTable({
                        "processing": true,
                        "serverSide": true,
                        "searchDelay": 1000,
                        "ajax": {
                            "url": "{{route('employee.datatable')}}",
                            "type": "POST",
                            "data": function ( d ) {
                                d._token = "{{csrf_token()}}"
                            
                            }
                        },
                        "columns":[
                            { "data": 'first_name'},
                            { "data": 'last_name'},
                            { "data": 'company_id'},
                            { "data": 'email'},
                            { "data": 'phone'},
                            { "data":  null}
                        ],
                        "columnDefs":[
                            {
                    targets:2,
                    render: (a, e, t, n) => {
                        return t.company ? t.company.company_name : "";
                    },
                },
                        
             {
                 targets:-1,
                 render: (a, e, t, n) => {
                     return `<a href="{{url('/').'/admin/employee/edit/'}}${t.employee_id}" class="btn btn-success "><i class="fa fa-pen" aria-hidden="true"></i></a>
                     <a href="{{url('/').'/admin/employee/delete/'}}${t.employee_id}" class="btn btn-danger "><i class="fa fa-trash aria-hidden="true""></i></a>`;
                 }
             }
        ]
                 });
                  

    });

</script>
@endsection