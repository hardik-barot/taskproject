<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model
{
    //tablename
    public $table='company';
    //primaryKey
    public $primaryKey='company_id';
    //timestamps
    public $timestamps=true;
}
