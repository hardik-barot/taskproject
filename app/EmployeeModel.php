<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeModel extends Model
{
    //tablename
    public $table='employee';
    //primaryKey
    public $primaryKey='employee_id';
    //timestamps
    public $timestamps=true;


    public function company()
    {
        return $this->belongsTo('App\CompanyModel', 'company_id', 'company_id');
    }
}
