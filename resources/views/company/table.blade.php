@extends('layout.main')
@section('content')
<div class="container-fluid">
                        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                    <a href="{{route('company.add')}}" class="btn btn-primary"><i class="fas fa-plus"></i> Add Company</a>
                    </div>
                    <h4 class="page-title">List Of Companies</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title"></h4>
                       

                        <table id="company_datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>Compnay Name</th>
                                    <th>Email</th>
                                    <th>Logo</th>
                                    <th>Website_Url</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>

           
@endsection
@section('scripts')
<script type="text/javascript">

        $(document).ready(function(){
                // get_songs();



                        let company_datatable = $('#company_datatable').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "searchDelay": 1000,
                            "ajax": {
                                "url": "{{route('company.datatable')}}",
                                "type": "POST",
                                "data": function ( d ) {
                                    d._token = "{{csrf_token()}}"
                                
                                }
                            },
                            "columns":[
                                { "data": 'company_name'},
                                { "data": 'email'},
                                { "data": 'logo'},
                                { "data": 'website_url'},
                                { "data":  null}
                            ],
                            "columnDefs":[
                                {
                                    targets:2,
                                     render: (a, e, t, n) => {
                                  return `
                                    <img src="/Taskproject/storage/upload_images/${t.logo}" width="50">
                                  `;
                            },
                                },
                            
                {
                    targets:-1,
                    render: (a, e, t, n) => {
                        return `<a href="{{url('/').'/admin/company/edit/'}}${t.company_id}" class="btn btn-success "><i class="fa fa-pen" aria-hidden="true"></i></a>
                        <a href="{{url('/').'/admin/company/delete/'}}${t.company_id}" class="btn btn-danger "><i class="fa fa-trash aria-hidden="true""></i></a>`;
                    }
                }
            ]
                     });
                      

        });

</script>
    
@endsection