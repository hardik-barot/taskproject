<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','loginController@index1')->name('loginpage')->middleware('guest');
Route::post('/login','loginController@login')->name('login');
Route::get('/logout','loginController@logout')->name('logout');

Route::prefix('admin')->group(function () {

    Route::group(['middleware' => 'auth'], function () {
        
    Route::get('/home','AdminController@index')->name('admin.home');

    #Company Routes
    Route::get('/compnay/add','CompanyController@add_company')->name('company.add');
    Route::post('/company/store','CompanyController@add_data')->name('company.store');
    Route::get('/company','CompanyController@table')->name('company.table');
    Route::get('/company/edit/{id}','CompanyController@edit_company')->name('edit.company');
    Route::post('/company/update','CompanyController@update_company')->name('update.company');
    Route::get('/company/delete/{id}','CompanyController@delete_company')->name('delete.company');
    Route::post('/company/datatable','CompanyController@compnayDatatable')->name('company.datatable');



    #Employee Routes
    Route::get('/employee/add','EmployeeController@add_employee')->name('employee.add');
    Route::post('/employee/store','EmployeeController@insert')->name('employee.store');
    Route::get('/employee/table','EmployeeController@table')->name('employee.table');
    Route::get('/employee/edit/{id}','EmployeeController@edit_employee')->name('edit.employee');
    Route::post('/employee/update','EmployeeController@update_employee')->name('update.employee');
    Route::get('/employee/delete/{id}','EmployeeController@delete_employee')->name('delete.employee');
    Route::post('/employee/datatable','EmployeeController@employeeDatatable')->name('employee.datatable');
    
    

    });
});