<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyModel;
use Validator;
use App\Common;

class CompanyController extends Controller
{
        public function add_company()
        {
            return view('company.add');
        }

        public function add_data(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'company_name' => 'required',
                'email'=>'required',
                'logo'=>'required',
                'website_url'=>'required'
            ]);
    
            if ($validator->fails()) {
                return redirect()->route('company.add')->withErrors($validator);
            }
            if ($request->hasFile('logo')) {

                $fileNameToStore = Common::fileUpload($request, 'logo');
            }

            $company=new CompanyModel;
            $company->company_name=$request->company_name;
            $company->email=$request->email;
            $company->logo=$fileNameToStore;
            $company->website_url=$request->website_url;
            $company->save();

            return redirect()->route('company.table')->with('success','Company Added Successfully');

          
        }
        public function table()
        {
            return view ('company.table');
        }

       public function compnayDatatable(Request $request)
       {
        $companies = CompanyModel::query();
        $recordsTotal = $companies->count();

                $searchParam = $request->search['value'] ?? "";
                $companies->when($searchParam, function($query) use($searchParam){
                return $query->where('company_name','like','%'.$searchParam.'%');
                });
                
                $recordsFiltered = $companies->get()->count();
                $companies->offset($request->start)->limit($request->length)->orderBy('company_id','DESC');

                $data = [
                    'draw' => $request->draw,
                    'recordsTotal' => $recordsTotal,
                    'recordsFiltered' => $recordsFiltered,
                    'data' => $companies->get()
                ];
    return $data;

    }

    public function edit_company($id)
    {
        $company=CompanyModel::find($id);
        return view('company.edit_company')->with('company',$company);
    }
    public function update_company(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'email'=>'required',
            'logo'=>'required',
            'website_url'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('edit.company',['id'=>$request->id])->withErrors($validator);
        }
        if ($request->hasFile('logo')) {

            $fileNameToStore = Common::fileUpload($request, 'logo');
        }
        $company=CompanyModel::find($request->id);
        if ($request->hasFile('logo')) {
            $company->logo = $fileNameToStore;
        }
        $company->company_name=$request->company_name;
        $company->email=$request->email;
        $company->website_url=$request->website_url;
        $company->save();
        return redirect()->route('company.table')->with('success','CompanyDetails Updated Successfully');
        
    }
    public function delete_company($id)
    {
        CompanyModel::find($id)->delete();
        return redirect()->route('company.table')->with('success','Deleted Successfully');
    }
}
