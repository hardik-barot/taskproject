<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanyModel;

class companyController extends Controller
{
    public function list(Request $request)
    {
        $company=CompanyModel::all();

        if(count($company)>0)
        {
            return [
                'msg'=>'List Of Companies',
                'status'=>1,
                'data'=>$company
            ];
        }
        else
        {
            return [
                'msg'=>'No Company Available',
                'status'=>0
            ];
        }
    }
}
